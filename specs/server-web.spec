Name:                           server-web
Version:                        1.0.1
Release:                        2%{?dist}
Summary:                        SERVER-package for install and configure WEB
License:                        GPLv3

Requires:                       meta-system meta-nginx meta-httpd meta-php meta-mysql meta-redis meta-elasticsearch

%description
SERVER-package for install and configure WEB.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%install


%files


%changelog
* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-2
- Update from MARKETPLACE.

* Sat Jun 22 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-1
- Change MariaDB to MySQL.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- New version: 1.0.0-2.

* Wed Feb 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
