# Information / Информация

SPEC-файл для создания RPM-пакета **server-web**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/common-server`.
2. Установить пакет: `dnf install server-web`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)